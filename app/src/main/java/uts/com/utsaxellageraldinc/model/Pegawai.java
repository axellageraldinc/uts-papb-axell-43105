package uts.com.utsaxellageraldinc.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Created by axellageraldinc on 07/04/18.
 */

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Pegawai {
    private String id, name;
}
