package uts.com.utsaxellageraldinc.admin;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;

import java.util.ArrayList;
import java.util.List;

import uts.com.utsaxellageraldinc.R;
import uts.com.utsaxellageraldinc.model.Pegawai;

public class AdminManajamenPegawaiActivity extends AppCompatActivity {

    private Toolbar toolbar;
    private RecyclerView recyclerViewNamaPegawai;
    private NamaPegawaiAdapter namaPegawaiAdapter;

    private List<Pegawai> pegawaiList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_admin_manajamen_pegawai);
        initWidgets();
        dummyData();
    }

    private void initWidgets(){
        initToolbar();
        initRecyclerView();
    }
    private void initToolbar(){
        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }
    private void initRecyclerView(){
        recyclerViewNamaPegawai = findViewById(R.id.recyclerViewNamaPegawai);
        namaPegawaiAdapter = new NamaPegawaiAdapter(pegawaiList);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerViewNamaPegawai.setLayoutManager(mLayoutManager);
        recyclerViewNamaPegawai.setItemAnimator(new DefaultItemAnimator());
        recyclerViewNamaPegawai.setAdapter(namaPegawaiAdapter);
    }

    private void dummyData(){
        pegawaiList.add(Pegawai.builder()
                .id("pegawai1")
                .name("Axell")
                .build());
        pegawaiList.add(Pegawai.builder()
                .id("pegawai2")
                .name("Azzum")
                .build());
        pegawaiList.add(Pegawai.builder()
                .id("pegawai3")
                .name("Raufi")
                .build());
        pegawaiList.add(Pegawai.builder()
                .id("pegawai4")
                .name("Yanuwar")
                .build());
        pegawaiList.add(Pegawai.builder()
                .id("pegawai5")
                .name("Pak Damas")
                .build());
        namaPegawaiAdapter.notifyDataSetChanged();
    }

}
