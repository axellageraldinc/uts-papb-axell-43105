package uts.com.utsaxellageraldinc.admin;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import uts.com.utsaxellageraldinc.R;

public class AdminHomeActivity extends AppCompatActivity {

    private Button btnJaga;
    private CardView cardViewManajemenPegawai;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_admin_home);
        initWidgets();
    }

    private void initWidgets(){
        initButtons();
        initCardViews();
    }
    private void initButtons(){
        btnJaga = findViewById(R.id.btnJaga);
        btnJaga.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(AdminHomeActivity.this, "Tombol ini akan diimplementasikan nanti...", Toast.LENGTH_SHORT).show();
            }
        });
    }
    private void initCardViews(){
        cardViewManajemenPegawai = findViewById(R.id.cardViewManajemenPegawai);
        cardViewManajemenPegawai.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), AdminManajamenPegawaiActivity.class);
                startActivity(intent);
            }
        });
    }

}
