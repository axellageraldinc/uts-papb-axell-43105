package uts.com.utsaxellageraldinc.admin;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import uts.com.utsaxellageraldinc.R;
import uts.com.utsaxellageraldinc.model.Pegawai;

/**
 * Created by axellageraldinc on 07/04/18.
 */

public class NamaPegawaiAdapter extends RecyclerView.Adapter<NamaPegawaiAdapter.NamaPegawaiHolder> {

    private List<Pegawai> pegawaiList;

    public NamaPegawaiAdapter(List<Pegawai> pegawaiList) {
        this.pegawaiList = pegawaiList;
    }

    @Override
    public NamaPegawaiHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.nama_pegawai_adapter, parent, false);

        return new NamaPegawaiHolder(itemView);
    }

    @Override
    public void onBindViewHolder(NamaPegawaiHolder holder, int position) {
        holder.txtNamaPegawai.setText(pegawaiList.get(position).getName());
    }

    @Override
    public int getItemCount() {
        return pegawaiList.size();
    }

    public class NamaPegawaiHolder extends RecyclerView.ViewHolder {
        public TextView txtNamaPegawai;

        public NamaPegawaiHolder(View view) {
            super(view);
            txtNamaPegawai = view.findViewById(R.id.txtNamaPegawai);
        }
    }

}
